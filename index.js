
const fs = require('fs');
const request = require('request');
const path = require('path');

let locationsArray = null;
const locations = [];
const dateThreshold = new Date('2019-01');
const MongoClient = require('mongodb').MongoClient;

const url = 'mongodb://localhost:27017/';
let database = null;
let panoramas = null;
const apiKey = 'AIzaSyAj5-5r2xqKsv3urn6kbuf9lr1EWnji-2E';

const setupDB = () => new Promise((resolve) => {
  MongoClient.connect(url, { useUnifiedTopology: true }, (err, db) => {
    if (err) return console.log(err);
    console.log('Node server connected to MongoDB');
    database = db.db('crowd-detector');
    panoramas = database.collection('panoramas');
    panoramas.find({}).toArray((err, results) => {
      locationsArray = results;
      resolve();
    });
    return null;
  });
});

const apiURL = 'https://maps.googleapis.com/maps/api/streetview';
const metaURL = `${apiURL}/metadata`;
const width = 512;
const height = 512;

const fov = 120;
const pitch = 0;

const downloadImage = image => new Promise((resolve) => {
  request(image.url).pipe(fs.createWriteStream(path.join('images', image.filename))).on('close', () => { resolve(); });
});

const downloadPanorama = location => new Promise((resolve) => {
  const promises = [];
  for (let heading = 0; heading < 360; heading += 90) {
    promises.push(downloadImage({
      url: `${apiURL}?size=${width}x${height}&location=${location.lat},${location.lng}&fov=${fov}&heading=${heading}&pitch=${pitch}&key=${apiKey}`,
      filename: `${location.lat}_${location.lng}_${heading}.jpg`,
    }));
  }

  Promise.all(promises).then(() => { resolve(); });
});


let count = 0;
const checkDate = location => new Promise((resolve) => {
  request(location.url, (err, res, body) => {
    count += 1;
    if (!err) {
      console.log(`received ${count} of ${locations.length} requests`);
      const { date } = JSON.parse(body);
      // console.log(date);
      location.date = new Date(date);
      if (location.date > dateThreshold) {
        downloadPanorama(location).then(() => {
          panoramas.insertOne(location, () => resolve());
        });
      } else {
        panoramas.insertOne(location, () => resolve());
      }
    } else {
      console.log(err);
    }
  });
});


const lat = 40.7292925;
const lng = -74.2103879;
const rows = 100;
const cols = 100;
const spacing = 0.0001;

const generateLinks = () => {
  const promises = [];


  for (let latOffset = 0; latOffset < (rows * spacing); latOffset += spacing) {
    for (let lngOffset = 0; lngOffset < (cols * spacing); lngOffset += spacing) {
    // for (let heading = 0; heading < 360; heading += 90) {

      const location = {
        lat: (lat + latOffset).toFixed(7),
        lng: (lng + lngOffset).toFixed(7),
        url: `${metaURL}?size=${width}x${height}&location=${lat + latOffset},${lng + lngOffset}&key=${apiKey}`,
        date: null,
      };

      let exists = false;
      for (let i = 0; i < locationsArray.length; i += 1) {
        if (locationsArray[i].lat === location.lat && locationsArray[i].lng === location.lng) {
          exists = true;
          break;
        }
      }
      if (!exists) {
        locations.push(location);
        promises.push(checkDate(location));
      }
    }
  }

  Promise.all(promises).then(() => {
    console.log('done');
  });
};

setupDB().then(generateLinks);
